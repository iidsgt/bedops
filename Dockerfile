# Remove ENTRYPOINT which is incompatible with CWL:
# https://github.com/common-workflow-language/common-workflow-language/issues/522
FROM dockerbiotools/bedops:latest
ENTRYPOINT []
